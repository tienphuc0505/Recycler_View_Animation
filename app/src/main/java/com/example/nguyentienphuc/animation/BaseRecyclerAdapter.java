package com.example.nguyentienphuc.animation;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public abstract class BaseRecyclerAdapter<M, VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {

    protected BaseViewHolder.OnItemRecyclerClick onItemRecyclerClick;
    protected BaseViewHolder.OnItemRecyclerLongClick onItemRecyclerLongClick;

    private ArrayList<M> items = new ArrayList<M>();

    public BaseRecyclerAdapter() {
        setHasStableIds(true);
    }

    public void setOnItemRecyclerClick(BaseViewHolder.OnItemRecyclerClick onItemRecyclerClick) {
        this.onItemRecyclerClick = onItemRecyclerClick;
    }

    public void setOnItemRecyclerLongClick(BaseViewHolder.OnItemRecyclerLongClick onItemRecyclerLongClick) {
        this.onItemRecyclerLongClick = onItemRecyclerLongClick;
    }

    public void add(M object) {
        items.add(object);
        notifyDataSetChanged();
    }

    public void add(int index, M object) {
        items.add(index, object);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends M> collection) {
        addAll(collection, true);
    }

    public void addAll(Collection<? extends M> collection, boolean isClear) {
        if (collection != null) {
            if (isClear) items.clear();
            items.addAll(collection);
            notifyDataSetChanged();
        }
    }

    public void addAll(M... items) {
        addAll(Arrays.asList(items));
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void remove(M object) {
        items.remove(object);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public M getItem(int position) {
        return items.get(position);
    }

    public ArrayList<M> getItems() {
        return items;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean contains(Object dto) {
        return items.contains(dto);
    }
}