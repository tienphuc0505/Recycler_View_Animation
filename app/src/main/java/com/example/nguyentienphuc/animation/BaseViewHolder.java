package com.example.nguyentienphuc.animation;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;


public abstract class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    OnItemRecyclerClick onRecylerItemClick;
    OnItemRecyclerLongClick onRecylerItemLongClick;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public BaseViewHolder(View itemView, OnItemRecyclerClick onRecylerItemClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
        this.onRecylerItemClick = onRecylerItemClick;
    }

    public BaseViewHolder(View itemView, OnItemRecyclerClick onRecylerItemClick, OnItemRecyclerLongClick onRecylerItemLongClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        this.onRecylerItemClick = onRecylerItemClick;
        this.onRecylerItemLongClick = onRecylerItemLongClick;
    }

    @Override
    public void onClick(View view) {
        if (onRecylerItemClick != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onRecylerItemClick.onItemClick(view, position);
            }
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (onRecylerItemLongClick != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onRecylerItemLongClick.onItemLongClick(view, position);
            }
        }
        return false;
    }

    public interface OnItemRecyclerClick {
        public void onItemClick(View view, int position);
    }

    public interface OnItemRecyclerLongClick {
        public void onItemLongClick(View view, int position);
    }
}
