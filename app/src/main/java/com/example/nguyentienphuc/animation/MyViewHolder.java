package com.example.nguyentienphuc.animation;

import android.view.View;

import butterknife.BindView;

/**
 * Created by nguyentienphuc on 6/1/17.
 */

public class MyViewHolder extends BaseViewHolder {
    @BindView(R.id.txtv)
    public CustomTextView txtv;

    public MyViewHolder(View itemView, OnItemRecyclerClick onRecylerItemClick) {
        super(itemView, onRecylerItemClick);
    }
}
