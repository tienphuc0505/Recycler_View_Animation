package com.example.nguyentienphuc.animation;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.transitionseverywhere.ChangeText;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final int W = 5;
    public static final int H = 6;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.number)
    TextView txtvNumber;

    MyAdapter myAdapter;

    int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        myAdapter = new MyAdapter();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        recyclerView.setAdapter(myAdapter);

        for (int i = 0; i < W * H; ++i) {
            myAdapter.add(new Data(String.valueOf(i)));
        }

        final Animation zoomOut = AnimationUtils.loadAnimation(getBaseContext(), R.anim.zoomout);

        myAdapter.setOnItemRecyclerClick(new BaseViewHolder.OnItemRecyclerClick() {
            @Override
            public void onItemClick(final View view, int position) {
                myAdapter.getItem(position).isChoosen = true;
                setUpCountAnimation();
                zoomOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        myAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                view.startAnimation(zoomOut);

            }
        });

        myAdapter.notifyDataSetChanged();

        setUpPullToRefresh();

    }

    void createAnimation(long delay, final View view) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new DecelerateInterpolator());
        fadeOut.setStartOffset(delay);
        fadeOut.setDuration(500);
        view.startAnimation(fadeOut);
    }

    void setUpPullToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int a = 1;
                int b = 1;

                for (int i = 0; i < W; ++i) {
                    for (int j = 0; j < a; ++j) {
                        createAnimation(a * 100, recyclerView.getChildAt(i + j * 4));
                    }
                    a++;
                    b++;
                }
                b--;
                for (int i = 1; i <= H - 1; ++i) {
                    int cur = ((i + 1) * 4) + i;
                    for (int j = 0; j < b; ++j) {
                        createAnimation(a * 100, recyclerView.getChildAt(cur + j * 4));

                    }
                    a++;
                    if (H - i <= W) {
                        b--;
                    }
                }
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    void setUpCountAnimation() {
        TransitionManager.beginDelayedTransition(container,
                new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN)
                        .setDuration(500));
        count++;
        txtvNumber.setText(String.valueOf(count));
    }
}
