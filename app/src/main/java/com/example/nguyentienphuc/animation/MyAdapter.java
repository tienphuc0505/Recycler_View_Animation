package com.example.nguyentienphuc.animation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by nguyentienphuc on 6/1/17.
 */

public class MyAdapter extends BaseRecyclerAdapter<Data, MyViewHolder> {
    static final int UNCHOOSEN = 0;
    static final int CHOOSEN = 1;

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isChoosen ? CHOOSEN : UNCHOOSEN;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == UNCHOOSEN) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_choosen, parent, false);
        }
        return new MyViewHolder(view, onItemRecyclerClick);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtv.setText(getItem(position).data);
    }
}
